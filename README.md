PagesIndexGen v0.1

## Intro

This is simple site index generator with templates.
It can be used in gitlab pages.

## Usage

~~~
./pagesindexgen.py filedir sitedir template
~~~

Any parameters can be avoided.

## Examples

This examples is same:

~~~
./pagesindexgen.py
./pagesindexgen.py .
./pagesindexgen.py . /
./pagesindexgen.py . / default
~~~

## Demo site directory

In source included demo directory with some files and script to run build.


## Url

 - [Home page](https://gitlab.com/4144/pagesindexgen)
